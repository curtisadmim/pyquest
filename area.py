from block import Block


class Area:
    x = 10
    y = 10
    block = Block

    def set_size(self, x, y):
        self.x = x
        self.y = y

    def gen_map(self):
        map = {}
        for i in range(self.x):
            for j in range(self.y):
                map[str(i+1) + "," + str(j+1)]= self.block.gen_default_block(self.block)
        return map

