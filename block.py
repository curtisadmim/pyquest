class Block:

    def gen_default_block(self):
        block = {}
        block['lou_level']='earth'
        block['high_level'] = 'air-normal'
        block['speed_mod'] = '1'
        block['melee_def_mod'] = '1'
        block['range_def_mod'] = '1'
        block['spell_def_mod'] = '1'
        block['melee_atk_mod'] = '1'
        block['range_atk_mod'] = '1'
        block['cast_atk_mod'] = '1'
        return block
