CREATE TABLE public.map_block (
	id varchar NOT NULL,
	map_id varchar NOT NULL,
	lou_level smallint NULL,
	high_level smallint NULL,
	speed_mod smallint NULL,
	melee_def_mod smallint NULL,
	range_def_mod smallint NULL,
	spell_def_mod smallint NULL,
	melee_atk_mod smallint NULL,
	long_melee_atk_mod smallint NULL,
	range_atk_mod smallint NULL
);
CREATE TABLE public."map" (
	id varchar NOT NULL,
	map_id varchar NOT NULL,
	"Name" varchar NOT NULL,
	"owner" varchar NULL
);
CREATE TABLE public.lou_level (
	id smallint NOT NULL,
	value smallint NOT NULL
);
COMMENT ON TABLE public.lou_level IS 'Описание поверхности.';
CREATE TABLE public.high_level (
	id smallint NOT NULL,
	value varchar NOT NULL
);
COMMENT ON TABLE public.high_level IS 'Описание окружения';
CREATE TABLE public.speed_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.speed_mod IS 'Модификатор затрат ОД при перемещение по данному полю';
CREATE TABLE public.melee_def_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.melee_def_mod IS 'Модификатор защиты в ближнем бою';
CREATE TABLE public.range_def_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.range_def_mod IS 'Модификатор защиты в дальнем бою';
CREATE TABLE public.spell_def_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.spell_def_mod IS 'Модификатор защиты от заклинаний';
CREATE TABLE public.melee_atk_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.melee_atk_mod IS 'Модификатор атаки в ближнем бою';
CREATE TABLE public.range_atk_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.range_atk_mod IS 'Модификатор атаки в дальнем бою';
CREATE TABLE public.cast_atk_mod (
	id smallint NOT NULL,
	value float4 NOT NULL
);
COMMENT ON TABLE public.cast_atk_mod IS 'Модификатор урона заклинаний';
ALTER TABLE public.high_level ADD description varchar NULL;
ALTER TABLE public.lou_level ALTER COLUMN value TYPE varchar USING value::varchar;
ALTER TABLE public.map_block ADD "position" varchar NOT NULL;
ALTER TABLE public.map_block RENAME COLUMN long_melee_atk_mod TO cast_atk_mod;



