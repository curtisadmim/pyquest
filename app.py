import json
from DBconector import connector
from flask import Flask

from area import Area
app = Flask(__name__)
map = Area()
con = connector()

@app.route('/')
def generate_map():
    map = Area()
    c_area = map.gen_map()
    c_area['id'] = con.getmapid()
    con.map_gen(c_area)
    return json.dumps(c_area)

# @app.route('/map/<int:map_id>')
# def get_map(map_id):
#     return json.dumps(map_list[map_id])


if __name__ == '__main__':
    app.run(debug=True)